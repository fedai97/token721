import React from 'react';
import {Route, Switch} from 'react-router-dom';
import CreateSalePage from "../pages/CreateSale";
import MarketPage from "../pages/Market";

export const Router = () => {
    return (
        <main className="content">
            <Switch>
               <Route exact path="/" component={MarketPage}/>
               <Route exact path="/create" component={CreateSalePage}/>
            </Switch>
        </main>
    )
};
