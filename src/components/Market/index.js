import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import useHandleMarket from "../../hooks/web3/useHandleMarket";
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        margin: '20px',
    },
    paper: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary,
    },
    input: {
        width: '100%',
    },
    buyBtn: {
        width: '100%',
    },
    card: {
        root: {
            minWidth: 275,
        },
        bullet: {
            display: 'inline-block',
            margin: '0 2px',
            transform: 'scale(0.8)',
        },
        title: {
            fontSize: 14,
        },
        pos: {
            marginBottom: 12,
        },
    }
}));

export default function Market(props) {
    const classes = useStyles();
    const {marketList, buyToken} = useHandleMarket(props);
    console.log('marketList --> ', marketList);

    return (
        <div className={classes.root}>
            {
                !!marketList.length
                && marketList.map((item, key) => (
                    <Card className={classes.card.root} variant="outlined" key={key}>
                        <CardContent>
                            <Typography variant="h5" component="h2">
                                {item}
                            </Typography>
                        </CardContent>
                        {/*<CardActions>*/}
                        {/*    {*/}
                        {/*        item.sold*/}
                        {/*            ? <Button size="small">Sold</Button>*/}
                        {/*            : <Button size="small" onClick={() => buyToken(item)}>Buy</Button>*/}
                        {/*    }*/}
                        {/*</CardActions>*/}
                    </Card>
                ))
            }
        </div>
    )
};
