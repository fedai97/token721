import {useEffect, useState} from "react";
import {useLocation} from 'react-router-dom';

export default function useHandleMarket({account, contract}) {
    const [marketList, setMarketList] = useState([]);
    const [soldMarketList, setSoldMarketList] = useState([]);

    const location = useLocation();

    async function createMarket() {
        createSale();
    }

    async function createSale() {
        await contract.methods.mint('Hello')
            .send({from: account})
            .once('receipt', (receipt) => {
            })
    }

    async function loadNFTs() {
        const totalSupply = await contract.methods.totalSupply().call()

        for (let i = 1; i <= totalSupply; i++) {
            const newColor = await contract.methods.colors(i - 1).call();
            setMarketList([...marketList, ...[newColor]]);
        }
    }

    async function buyToken(token) {
        // const web3 = window.web3;
        //
        // const contractNFTMarket = new web3.eth.Contract(NFTMarket.abi, nftmarketaddress);
        // const price = +token.price * 1000000000000000;
        // console.log('token.price --> ', price.toString());
        //
        // await contractNFTMarket.methods
        //     .createMarketSale(nftaddress, token.tokenId)
        //     .send({from: account, value: price.toString()});
        //
        // setMarketList([]);
        // setSoldMarketList([]);
        // loadNFTs()
    }

    useEffect(() => {
        setMarketList([]);
        setSoldMarketList([]);

        if (!location.pathname.split('/')[1].length && contract) {
            loadNFTs();
        }
    }, [location, contract])

    return {
        marketList, soldMarketList,
        createMarket, loadNFTs, buyToken,
    }
}