import {useEffect, useState} from "react";
import Web3 from 'web3';
import Contract from '../../abis/NFT.json';

export default function useInitWeb3() {
    const [account, setAccount] = useState('');
    const [contract, setContract] = useState(null);
    const [ethBalance, setEthBalance] = useState('');

    const loadWeb3 = async () => {
        if (window.ethereum) {
            window.web3 = new Web3(window.ethereum);
            await window.ethereum.enable();
        } else if (window.web3) {
            window.web3 = new Web3(window.web3.currentProvider)
        } else {
            window.alert("Non ethereum!")
        }
    };

    const loadBlockchainData = async () => {
        const web3 = window.web3;

        const accounts = await web3.eth.getAccounts();
        setAccount(accounts[0]);

        const ethBalance = await web3.eth.getBalance(accounts[0]);
        setEthBalance(ethBalance);

        const networkId = await web3.eth.net.getId()
        const networkData = Contract.networks[networkId]
        if (networkData) {
            const abi = Contract.abi;
            const address = networkData.address;
            setContract(new web3.eth.Contract(abi, address));
        }
    };

    useEffect(() => {
        (async function () {
            await loadWeb3();
            await loadBlockchainData();
        })()
    }, []);

    return {
        account, ethBalance, contract
    }
}