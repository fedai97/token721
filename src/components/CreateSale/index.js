import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import useHandleMarket from "../../hooks/web3/useHandleMarket";

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        margin: '20px',
    },
    paper: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary,
    },
    input: {
        width: '100%',
    },
    buyBtn: {
        width: '100%',
    },
}));

export default function CreateSale(props) {
    const classes = useStyles();
    const handleMarket = useHandleMarket(props);

    return (
        <div className={classes.root}>
            <div>
                Balance [ETH]: {window.web3.utils.fromWei(props.ethBalance, 'Ether')}
            </div>
            <button onClick={handleMarket.createMarket}>CLICK</button>
        </div>
    )
};
