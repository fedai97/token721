import React from "react";
import {useHistory} from 'react-router-dom';
import clsx from 'clsx';

import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import AppBar from "@material-ui/core/AppBar";
import SwipeableDrawer from '@material-ui/core/SwipeableDrawer';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import {makeStyles} from "@material-ui/core/styles";
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import StorefrontIcon from '@material-ui/icons/Storefront';
import {ListItemIcon} from "@material-ui/core";
import AddBoxIcon from '@material-ui/icons/AddBox';

const useStyles = makeStyles(() => ({
    title: {
        marginLeft: '20px',
        flexGrow: 1,
    },
}));

export default function Navbar({title, account}) {
    const classes = useStyles();
    const history = useHistory();

    const [state, setState] = React.useState({
        top: false,
        left: false,
        bottom: false,
        right: false,
    });

    const redirect = (to) => {
        switch (to) {
            case 'MARKET':
                return history.push('/');
            case 'CREATE':
                return history.push('/create');
            default:
                return;
        }
    };

    const toggleDrawer = (anchor, open) => (event) => {
        if (event && event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
            return;
        }

        setState({...state, [anchor]: open});
    };

    const list = (anchor) => (
        <div
            className={clsx(classes.list, {
                [classes.fullList]: anchor === 'top' || anchor === 'bottom',
            })}
            role="presentation"
            onClick={toggleDrawer(anchor, false)}
            onKeyDown={toggleDrawer(anchor, false)}
        >
            <List>
                <ListItem button onClick={() => redirect('MARKET')}>
                    <ListItemIcon><StorefrontIcon/></ListItemIcon>
                    <ListItemText primary={"MARKET"}/>
                </ListItem>
                <ListItem button onClick={() => redirect('CREATE')}>
                    <ListItemIcon><AddBoxIcon/></ListItemIcon>
                    <ListItemText primary={"CREATE SALE"}/>
                </ListItem>
            </List>
        </div>
    );

    return (
        <AppBar position="static">
            <Toolbar>
                <IconButton edge="start" className={classes.menuButton} color="inherit" aria-label="menu"
                            onClick={toggleDrawer('left', true)}>
                    <MenuIcon/>
                </IconButton>
                <Typography variant="h6" className={classes.title}>
                    {title}
                </Typography>
                <Button color="inherit">{account}</Button>
            </Toolbar>
            <SwipeableDrawer
                anchor={'left'}
                open={state['left']}
                onClose={toggleDrawer('left', false)}
                onOpen={toggleDrawer('left', true)}
            >
                {list('left')}
            </SwipeableDrawer>
        </AppBar>
    )
}