import React from "react";
import {makeStyles} from "@material-ui/core/styles";

import Navbar from "../../components/common/Navbar";
import useInitWeb3 from "../../hooks/web3/useInitWeb3";
import Market from '../../components/Market';

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    menuButton: {
        marginRight: theme.spacing(2),
    },
    title: {
        flexGrow: 1,
    },
}));

export default function MarketPage() {
    const classes = useStyles();

    const initWeb3 = useInitWeb3();

    return <div className={classes.root}>
        <Navbar account={initWeb3.account} title={'Market'}/>
        {
            initWeb3.ethBalance
            && <Market {...initWeb3}/>
        }
    </div>
}