// SPDX-License-Identifier: MIT OR Apache-2.0
pragma solidity ^0.5.0;

import "./ERC721Full.sol";

contract NFT is ERC721Full {
    string[] public words;
    mapping(string => bool) _wordExists;

    constructor() ERC721Full("Word", "WRD") public {
    }

    function mint(string memory _word) public {
        require(!_wordExists[_word]);

        uint _id = words.push(_word);
        _mint(msg.sender, _id);
//        _setTokenURI(newItemId, tokenURI);
        _wordExists[_word] = true;
    }
}